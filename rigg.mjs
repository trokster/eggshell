import Gun from "gun";
import 'gun/lib/then.js';
import 'gun/lib/webrtc.js';
// import 'gun/axe.js';
import 'gun/sea.js';
import { config } from 'dotenv';

import { ChatOpenAI } from "langchain/chat_models/openai";
import { HumanChatMessage, SystemChatMessage } from "langchain/schema";
import { ConsoleCallbackHandler } from "langchain/callbacks";
import { get_encoding, encoding_for_model } from "@dqbd/tiktoken";
import { readFile } from 'fs/promises';

config();

const models = {
    "experAI": new ChatOpenAI({
        openAIApiKey: process.env.OPENAI_API_KEY,
        temperature: 0,
        modelName: "gpt-4",
        // verbose: true,
    }),
    "fastAI": new ChatOpenAI({
        openAIApiKey: process.env.OPENAI_API_KEY,
        temperature: 0,
        modelName: "gpt-3.5-turbo",
        // verbose: true,
    }),
};


import { Configuration, OpenAIApi } from "openai";

const configuration = new Configuration({
    apiKey: process.env.OPENAI_API_KEY,
});
const openai = new OpenAIApi(configuration);


import cl100k_base from "@dqbd/tiktoken/encoders/cl100k_base.json" assert { type: "json" };
import { Tiktoken } from "@dqbd/tiktoken/lite";

let ai_models = null;

openai.listModels().then((response) => {
    // console.log(response.data);
    ai_models = response.data.data;
    // for (let i = 0; i < ai_models.length; i++) {
    //     console.log(ai_models[i].id);
    // }

    // login xkayp
    gun.user().auth(process.env.ADMIN_USER, process.env.ADMIN_PASSWORD, function (ack) {
        if (!ack.err) {
            console.log("User logged in, starting monitoring...");
            watch();
        } else {
            console.log("Error ? " + ack.err);
        }
    });


});



let relays = [];
relays.unshift("https://gun.xcayp.com/gun");


let gun = Gun({ peers: relays, localStorage: false, file: "./data_agi", });

// console.log(JSON.stringify("Root: " + process.env.ADMIN_USER, null, ' '));

const askAI = async function (activity_type, user_prompt, context, verbose) {

    let system_prompt = "", instruction_prompt = "", token_limit = 0, temperature = 0, model = "";

    if (activity_type == "split") {
        temperature = 0.0;
        model = "fastAI";
        // let enc = encoding_for_model(models["fastAI"].modelName);
        system_prompt = "You are a benevolent AI, intent on splitting tasks into bite sized chunks.";
        instruction_prompt = "Derive a concise todo list to go from here. High level only, in the form of a js array without formatting.";
        token_limit = 4000;
    } else {
        console.log("ERROR could not handle: " + activity_type);
        return;
    }

    const encoding = new Tiktoken(
        cl100k_base.bpe_ranks,
        cl100k_base.special_tokens,
        cl100k_base.pat_str
    );

    let encoded_user_prompt = encoding.encode(user_prompt);
    let encoded_context = encoding.encode(context);
    let encoded_system_prompt = encoding.encode(system_prompt);
    let encoded_instruction_prompt = encoding.encode(instruction_prompt);
    encoding.free();

    if ((encoded_user_prompt.length + encoded_context.length + encoded_system_prompt.length + encoded_instruction_prompt.length) > token_limit) {
        console.log("WARNING: Trying to summarize first.");
        models["fastAI"].temperature = 0;
        let response = await models.fastAI.call([
            new SystemChatMessage("Summarize user text as much as possible, while keeping all the information intact."),
            new HumanChatMessage(context),
        ]);

        console.log("Summary is: " + response.text);
        context = response.text;
        return;
    }

    if (verbose) {
        console.log("Prompt: " + JSON.stringify([system_prompt,context + user_prompt, instruction_prompt], null, ' '));
    }

    models[model].temperature = temperature;
    let response = await models[model].call([
        new SystemChatMessage(system_prompt),
        new HumanChatMessage(context + user_prompt),
        new HumanChatMessage(instruction_prompt),
    ]);




    console.log("OK response is: " + response.text);

}


let watch = function () {
    console.log("Rigg: I am starting my overwatch...");
    // get users root node
    gun.get("~" + gun.user()._.sea.pub).get("agi").map().on(function (data, _key, as) {
        var key = _key || ((data || {})._ || {})['#'] || as.via.soul;
        let context = "";
        if (data === null) return;
        if (!data.status) data.status = "idle";
        if (!data.activity_type) data.activity_type = "split";
        console.log("Rigg: I see movement: " + key + " :: " + data.status + " :: " + data.activity_type);

        if (data.status == "todo") {
            gun.get("~" + gun.user()._.sea.pub).get("agi_context").get(key).once(async function (data2) {
                if (data2) {
                    console.log("Rigg: I see context: " + JSON.stringify(data, null, ' '));
                    context = data2;
                } else {
                    console.log("No context");
                }
                askAI(data.activity_type, data.prompt, context, true);
            });
            console.log(JSON.stringify(data, null, ' '));
        }

        // console.log(JSON.stringify(data.status, null, ' '));
    });
}



