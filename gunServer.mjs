import express from "express";

import Gun from "gun";
import 'gun/lib/then.js';
import 'gun/lib/webrtc.js';
// import 'gun/axe.js';
import 'gun/sea.js';

import getUrls from 'get-urls';
import fetch from 'node-fetch';

var log = function (msg) {
    console && console.log('\x1b[36m%s\x1b[0m', msg);
}

const {NODE_ENV} = process.env;
const GUN_PORT = process.env.GUN_PORT !== undefined ? process.env.GUN_PORT : 8765;

const ADMIN_PUB = "yE4h0aLABU3RBc1vHUhJ2xfTqLK6hqn0b1X6CnpPfCo.9PCmkQvhze0uZopa138UQDZGrpxVN7EEgKadGnhYYwI";

// log("READ CONF: " + JSON.stringify(process.env, null, '  '));

let gun = null;

log("GUN VERSION IS: " + Gun.version);
log("GUN_PORT IS: " + GUN_PORT);

const dev = NODE_ENV === "development";

var app = express();

// Gun.on("opt", function(ctx){

//   ctx.on("put", async function(msg){

//     let no = function(why){ ctx.on('in', {'@': msg["#"], err: msg.err = why}) };

//     let to = this.to;
//     var eve = this, at = eve.as, put = msg.put, soul = put['#'], key = put['.'], val = put[':'], state = put['>'], id = msg['#'], tmp;

//     log("WIRE::PUT: " + JSON.stringify(msg, null, ' '));

//     // soul can be of form xxxxxxx~soul
//     if(soul.split("~").length > 0) {
//       soul = "~" + soul.split("~")[1];
//     }
//     console.log("Processed soul: " + soul.split("/")[0]);


//     // If path starts width ADMIN_PUB, we allow
//     if(soul.split("/")[0] === "~"+ADMIN_PUB) {
//       log("ADMIN PUB PUT: " + JSON.stringify(msg, null, ' '));
//       return to.next(msg);
//     }
//     // We allow aliases
//     // TODO : ( Should really allow aliases pointing to allowed users )
//     if(soul.split("/")[0].startsWith("~@")) {
//       log("ALIAS PUT: " + JSON.stringify(msg, null, ' '));
//       return to.next(msg);
//     }

//     // If Ygg is not initialized, we ignore
//     console.log("Getting soul: "+ "~"+ADMIN_PUB+"/users");
//     let allowed = await getPromisedSoul("~"+ADMIN_PUB+"/users");
//     // console.log("Fetched: " + JSON.stringify(allowed, null, " "));
//     if(!allowed) {
//       log("Ygg not ready, or no registerd users, ignoring...");
//       return;
//     }
    
//     // If user is not registered, we ignore
//     if(soul.startsWith("~") && allowed[soul.split("/")[0]]) {
//       // Allowing user node
//       log("USER NODE PUT: " + JSON.stringify(msg, null, ' '));
//       return to.next(msg);
//     }

//     log("PUT REFUSED: " + JSON.stringify(msg, null, ""));

//     return;

//   });

//   this.to.next(ctx);
  
// });


app.use(Gun.serve);
  
var server = app.listen(
  GUN_PORT,
  function (err) {
    if (err) {
        log("error", err);
    }else {
        log("Listening on port: " + GUN_PORT);
    }
  }
);

async function fetchRelays() {
  let tmpRelays = []
  let res = await fetch(
    'https://raw.githubusercontent.com/wiki/amark/gun/volunteer.dht.md'
  )
  let data = await res.text()
  // log("Got this data: " + data);
  let urls = getUrls(data)

  urls = Array.from(urls)
  urls.forEach((u) => {
    let testUrl = new URL(u)

    if (testUrl.pathname === '/gun' && testUrl.pathname.indexOf('~~') === -1) {
      tmpRelays.push(testUrl.href)
    }
  })

  return tmpRelays
}

// fetchRelays().then((data) => {
//   // log("Peers: " + JSON.stringify(data));
  gun = Gun({
      file: "./data_gun",
      // peers: data,
      web: server
  });
// }).catch((e) => {
//   log("Error fetching relays: " + e);
//   gun = Gun({
//       file: "./data_gun",
//       web: server
//   });
// });

// var getSoul = (function () {
  
//   let cached = {};
//   const max_stack_size = 1000;

//   return function (soul, cb) {

//     if (!(cb instanceof Function)) cb = function () {};
//     if (cached[soul]) { //null if node does not exist, but has been queried and sub is set
//       cached[soul].called++;
//       cb.call(this, cached[soul].item);
//     } else {
//       let ff = function (data) {
//         cached[soul].item = data;
//       };
//       //log('gun')
//       log("Fetching soul: " + soul);
//       gun.get(soul).get(function (messg, eve) { //check existence
//         eve.off();
//         try {
//           console.log("Setting soul: " + soul + " to: " + JSON.stringify(messg, null, ""));
//         } catch(e) {
//           try {
//             console.log("Circular, trying put only: " + JSON.stringify(messg.put, null, ""));
//           } catch(e) {
//             console.log("Setting soul: " + soul + " to a value with a circular reference");
//           }
//         }
        
//         cached[soul] = {
//           called: 0,
//           change: ff,
//           item: messg.put || null //non-undefined in case no data, but still falsy
//         }
//         cb.call(this, messg.put);

//         // Do a fifo remove until we reach max_stack_size
//         let keys = Object.keys(cached);
//         if (keys.length > max_stack_size) {
//           log("MAX STACK SIZE REACHED!!!");
//           // keys.sort(function(a,b){return a.called > b.called ? -1 : a.called < b.called ? 1 : 0;});
//           while (Object.keys(cached).length > (max_stack_size - 5)) {
//             let key = Object.keys(cached)[Math.floor(Math.random(Object.keys.length))];
//             log("POP POP!!: " + key);
//             gun.get(key).off(cached[key].change);
//             delete(cached[key]);
//           }
//         }
//       });

//       gun.get(soul).on(ff);
//     }
//   }
// })();

// var getPromisedSoul = function(soul){
//   return new Promise(function(resolve) {
//     getSoul(soul, function(result){
//       //log("PROMISE: " + JSON.stringify(result));
//       resolve(result);
//     });
//   });
// }

