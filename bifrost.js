const polka = require('polka');
const { json } = require('body-parser');
const { exec } = require("child_process");
require('dotenv').config()
const fs = require('fs');

BIFROST_READ = "CxznniNsXhw7C_yeyJio";

let port = 7447;

let p = polka();
p.use(json());

tracker = {};
// All right, this is our rodeo
// let's check that all containers are up :)
try {
    tracker = JSON.parse(fs.readFileSync('build_tracker.json'));
} catch(e) {
    console.log("No tracker file");
}
// console.log("Current tracker: " + JSON.stringify(tracker));

// For each branch in tracker, we up docker
Object.keys(tracker).forEach(function(branch){

    console.log("Launching: docker-compose --file "+tracker[branch].ref_dir+"/deployments/"+tracker[branch].name+"/docker-compose.yml up -d");
    exec("docker-compose --file "+tracker[branch].ref_dir+"/deployments/"+tracker[branch].name+"/docker-compose.yml up -d", (error, stdout, stderr) => {
        if (error) {
            console.log(`error: ${error.message}`);
            return;
        }
        if (stderr) {
            console.log(`${stderr}`);
            // return;
        }
        console.log(`${stdout}`);    
    });
});

p.get('/', function(req, res) {
    res.end(`You hit root path...`);
});

p.post('/', function(req, res) {
    // console.log("Received: " + JSON.stringify(req, null, '  '));
    //console.log("Received: " + JSON.stringify(res, null, '  '));
    // console.log("Received a request: " + JSON.stringify(req.body, null, '  '));

    res.end(`Rigg: I heard you... Lets Check this thing...`);

    // console.log("Checking if we have correct signature.");
    // console.log("PROCESS env: " + process.env.GITLAB_TOKEN);

    // console.log("CHECKING001: " + (req.headers["x-gitlab-token"] == process.env.GITLAB_TOKEN) );
    // console.log("CHECKING002: " + (req.body.repository.name == "eggshell"));
    // console.log("CHECKING003: " + (req.body.event_name == "push"));
    // console.log("CHECKING004: " + (req.body.ref == "refs/heads/master"));

    if(req.headers["x-gitlab-token"] == process.env.GITLAB_TOKEN && req.body.repository && req.body.repository.name == "Yggdrasil" && req.body.build_name == "stable" && req.body.build_status == "success") {

        console.log("*******************************************");
        console.log("********* Yggdrasil has Changed ***********");
        console.log("************** Ragnarok! ******************");
        console.log("*******************************************");

        // login to docker
        exec("docker login registry.gitlab.com -u bifrost -p " + BIFROST_READ, (error, stdout, stderr) => {
            if (error) {
                console.log(`error: ${error.message}`);
                return;
            }
            if (stderr) {
                console.log(`${stderr}`);
                // return;
            }
            console.log(`${stdout}`);

            // Pull latest image of Yggdrasil
            exec("docker pull registry.gitlab.com/trokster/xkayp-skeleton:stable", (error, stdout, stderr) => {
                if (error) {
                    console.log(`error: ${error.message}`);
                    return;
                }
                if (stderr) {
                    console.log(`${stderr}`);
                    return;
                }
                console.log(`${stdout}`);
                // TODO : kill everything and restart
                console.log("RAGNAROK!!!");
            });
        });        
        

    } else if(req.headers["x-gitlab-token"] == process.env.GITLAB_TOKEN && req.body.repository && req.body.repository.name == "eggshell" && req.body.event_name == "push" && req.body.ref == "refs/heads/master") {
        console.log("  *****************   GIT FETCH  *****************   ");
        exec("git stash", (error, stdout, stderr) => {
            if (error) {
                console.log(`error: ${error.message}`);
                return;
            }
            if (stderr) {
                console.log(`${stderr}`);
                // return;
            }
            console.log(`${stdout}`);

            exec("git pull", (error, stdout, stderr) => {
                if (error) {
                    console.log(`error: ${error.message}`);
                    return;
                }
                if (stderr) {
                    console.log(`${stderr}`);
                    // return;
                }
                console.log(`${stdout}`);
        
                console.log("  *****************   NPM INSTALL  *****************   ");
                exec("npm install", (error, stdout, stderr) => {
                    if (error) {
                        console.log(`error: ${error.message}`);
                        return;
                    }
                    if (stderr) {
                        console.log(`${stderr}`);
                        // return;
                    }
                    console.log(`${stdout}`);
        
                    console.log("  *****************   BIFROST RESTART  *****************   ");
                    exec("pm2 restart bifrost.js", (error, stdout, stderr) => {
                        if (error) {
                            console.log(`error: ${error.message}`);
                            return;
                        }
                        if (stderr) {
                            console.log(`${stderr}`);
                            // return;
                        }
                        console.log("  *****************   GUN RESTART  *****************   ");
                        exec("pm2 restart gunServer.mjs", (error, stdout, stderr) => {
                            if (error) {
                                console.log(`error: ${error.message}`);
                                return;
                            }
                            if (stderr) {
                                console.log(`${stderr}`);
                                return;
                            }
                            console.log("  *****************   AAAAND we're set :) **************    ");
                        });
                    });
                });        
            });
        });
    } else if(req.headers["x-gitlab-token"] == process.env.GITLAB_TOKEN && req.body.repository && req.body.repository.name == "mundane-egg" && req.body.event_name == "push") {
        console.log("  *****************   HEARD A MUNDANE EGG PUSH  *****************   ");
        let branch = req.body.ref.split("/").pop();

        // Now let's analyze it
        let params = branch.split("-");
        console.log("Analyzing branch: " + JSON.stringify(params) );
        if(params[0] != "deploy") {
            // skip it, not of importance to us
            console.log("Not a deployment, ignoring...");
            return;
        }

        console.log("");
        console.log("*******************************************");
        console.log("***** Yggdrasil'leaves are rustling *******");
        console.log("*******************************************");
        console.log("");

        if(req.body.checkout_sha === null) {
            console.log("Branch has been deleted : " + branch);
        } else {
            console.log("Branch is moving: " + branch);
            // Launch setup via python
            var child = require('child_process').exec("python3 Yggdrasil.py " + branch);
                child.stdout.pipe(process.stdout);
                child.on('exit', function() {
                process.exit();
            })
            // exec("python3 Yggdrasil.py " + branch, (error, stdout, stderr) => {
            //     if (error) {
            //         console.log(`error: ${error.message}`);
            //         return;
            //     }
            //     if (stderr) {
            //         console.log(`${stderr}`);
            //         return;
            //     }
            //     console.log("Process log:");
            //     console.log(`${stdout}`);
            // });
            console.log("Sheeet");
            
        }
        console.log("OK We're done here :)");
        // console.log("HEADERS : " + JSON.stringify(req.headers, null, '  '));
        // console.log("Event : " + JSON.stringify(req.body, null, '  '))

    } else {
        if(process.env.DEBUG) {
            console.log("Event not recognized, gonna ignore: " + JSON.stringify(req.body, null, '  '))
            console.log("HEADERS: " + JSON.stringify(req.headers, null, '  '));
        } else {
            console.log("Ignoring a webhook, turn on debug in .env if details reqiuired.");
        }
    }

});

p.get('/:pth', function(req, res) {
    res.end(`PTH: ${req.params.pth}`);
});

p.listen(port, err => {
    if (err) throw err;
    console.log(`> Running on localhost:`+port);
});
