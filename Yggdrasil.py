import json
import sys
import re
import os
import subprocess
import shutil

def git(*args):
    return subprocess.check_call(['git'] + list(args))

def dockercompose(*args):
    return subprocess.check_call(['docker-compose'] + list(args))

def systemctl(*args):
    return subprocess.check_call(['systemctl'] + list(args))

def launch_setup(branch):
    
    ref = os.getcwd()
    
    print("Processing: %s" % (branch,))
    try:
        typo, app_port, gun_port, name = branch.split("-")
        
        v = re.compile("\d{4}$")
        if v.match(app_port) is None:
            print("Error: app port is not in the right format: 4 numbers(XXXX) vs  %s" % app_port)
            raise ValueError("APP PORT is not 4 digits")
        if v.match(gun_port) is None:
            print("Error: gun port is not in the right format:4 numbers(XXXX) vs  %s" % gun_port)
            raise ValueError("GUN PORT is not 4 digits")
        
        if app_port == gun_port:
            print("Error: both port can't be assigned to the same")
            raise ValueError("GUN PORT is the same as APP PORT: %s" % app_port)
            
        
        print("Checking if directory exists, creating otherwise")
        if not os.path.exists("deployments"):
            os.mkdir("deployments")

        # Add network check and creation
        print("Checking if docker network exists, creating otherwise")
        try:
            subprocess.check_output(['docker', 'network', 'inspect', 'xkayp_network'], stderr=subprocess.STDOUT)
            print("Network exists")
        except subprocess.CalledProcessError:
            print("Creating docker network: xkayp_network")
            subprocess.check_call(['docker', 'network', 'create', 'xkayp_network'])

        os.chdir("deployments")

        if os.path.exists(name):
            print("Directory existed, cleaning up: %s" % name)
            os.chdir(name)
            try:
                dockercompose("down")
            except:
                print("Warning: docker-compose down failed, continuing...")
                pass
            os.chdir("..")
            shutil.rmtree(name)
        
        print("Cloning mundane egg")
        # This is read only, minimal security issues
        git("clone", "https://oauth2:glpat-nDsiFCNxwRcYJKcssAL1@gitlab.com/trokster/mundane-egg", name)

        os.chdir(name)
        print("switching to target branch: %s" % (branch,))
        git("checkout", branch)
        git("pull")
        print("OK should have downloaded the branch")
        os.chdir("..")
        # check if a .env file exists for target
        env_file = f"{ref}/.env.{name}"
        target_env = f"{ref}/deployments/{name}/app/.env"

        if os.path.exists(env_file):                                                                                                                                                                     
            print(f"Found environment file for {name}, copying to deployment")                                                                                                                           
            shutil.copy2(env_file, target_env)                                                                                                                                                           
        else:                                                                                                                                                                                            
            print(f"Warning: No environment file found at {env_file}")                                                                                                                                   

        print("Writing docker yml")
        gun_data = "%s/data-%s" % (os.getcwd(), name)
        docker_yml = """
version: '3'
services:
  svelte:
    build: .
    ports:
    - "%s:3000"
    - "%s:7447"
    networks:
      - xkayp_network
    command:
      - /bin/bash
      - -c
      - |
        cd /build/app
        npm install
        npm audit fix
        npm run build
        pm2 start "gunServer.mjs"
        pm2 start "gunServer.js"
        pm2 start "build/index.js"
        tail -F anything
    volumes:
      # gun
      - %s/:/build/app/data/:rw

networks:
  xkayp_network:
    external: true
        """ % ( app_port, gun_port, gun_data )
        print("Docker YML : %s" % docker_yml)

        print("Writing yml file")
        with open("%s/deployments/%s/docker-compose.yml" % (ref, name), "w") as ff:
            ff.write(docker_yml)

        tracker = {}
        tgt_tracker = "%s/build_tracker.json" % ( ref, )
        try:
            if os.path.exists(tgt_tracker):
                with open(tgt_tracker, "r") as ff:
                    tracker = json.load(ff)
        except:
            print("Error reading json parameter: %s" % tgt_tracker)
        
        print("Current tracker: %s" % json.dumps(tracker, indent=4))

        tracker[branch] = {
            "ref_dir" : ref,
            "app_port": app_port,
            "gun_port": gun_port,
            "gun_data": gun_data,
            "name"    : name
        }

        with open(tgt_tracker, "w") as ff:
            json.dump(tracker, ff)

        print("Saved tracker: %s" % json.dumps(tracker, indent=4))

        print("Setting up the ginx")
        
        #TODO move this elsewhere so we can work for different initial nginx configurations
        #keep it for now while we have a unique server
        nginx = """
# Default server configuration
#
server {
        listen 80 default_server;
        listen [::]:80 default_server;
    return 301 https://$host$request_uri;
}

server {

    listen 443 ssl;
    server_name *.xcayp.com;

    ssl_certificate /etc/letsencrypt/live/xcayp.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/xcayp.com/privkey.pem;
    # include /etc/letsencrypt/options-ssl-nginx.conf;
    # ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;

        root /var/www/html;

        # Add index.php to the list if you are using PHP
        index index.html index.htm index.nginx-debian.html;

        #deny access to .htaccess files, if Apache's document root
        #concurs with nginx's one

        location ~ /\.ht {
                deny all;
        }
}

server {
    listen 443 ssl;
    server_name bifrost.xcayp.com;

    ssl_certificate /etc/letsencrypt/live/xcayp.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/xcayp.com/privkey.pem;
    #include /etc/letsencrypt/options-ssl-nginx.conf;
    # ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;

        location / {
               proxy_pass http://localhost:7447;
               proxy_http_version 1.1;
               proxy_set_header Upgrade $http_upgrade;
               proxy_set_header Connection 'upgrade';
               proxy_set_header Host $host;
               proxy_cache_bypass $http_upgrade;
        }
}

server {
    listen 443 ssl;
    server_name gun.xcayp.com;

    ssl_certificate /etc/letsencrypt/live/xcayp.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/xcayp.com/privkey.pem;
    #include /etc/letsencrypt/options-ssl-nginx.conf;
    # ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;

        location / {
            root /var/www/html;
            index index.html index.htm index.nginx-debian.html;
        }

        location /gun {
               proxy_pass http://localhost:8765;
               proxy_http_version 1.1;
               proxy_set_header Upgrade $http_upgrade;
               proxy_set_header Connection 'upgrade';
               proxy_set_header Host $host;
               proxy_cache_bypass $http_upgrade;
        }
}

        """

        for branch in tracker:
            nginx += """

server {
    listen 443 ssl;
    server_name %s;

    ssl_certificate /etc/letsencrypt/live/xcayp.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/xcayp.com/privkey.pem;
    #include /etc/letsencrypt/options-ssl-nginx.conf;
    # ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;

        location / {
               proxy_pass http://localhost:%s;
               proxy_http_version 1.1;
               proxy_set_header Upgrade $http_upgrade;
               proxy_set_header Connection 'upgrade';
               proxy_set_header Host $host;
               proxy_cache_bypass $http_upgrade;
        }
        location /gun {
                        proxy_pass http://localhost:%s;
                        proxy_http_version 1.1;
                        proxy_set_header Upgrade $http_upgrade;
                        proxy_set_header Connection 'upgrade';
                        proxy_set_header Host $host;
                        proxy_cache_bypass $http_upgrade;
        }
}

            """ % (tracker[branch]["name"], tracker[branch]["app_port"], tracker[branch]["gun_port"])

        print("OK nginx entry is: %s" % nginx)
        
        try:
            print("Writing nginx")
            with open("/etc/nginx/sites-available/default", "w") as ff:
                ff.write(nginx)

            print("Restarting nginx")
            systemctl("reload", "nginx")
        except:
            print("There was an issue restarting nginx")

        print("(Re) starting docker");
        os.chdir("%s/deployments/%s" % (ref, name))
        dockercompose("up", "--build", "-d")
        
        print("Cleaning up unused docker images...")
        subprocess.check_call(['docker', 'image', 'prune', '-f'])
        
        # Check disk usage
        df_output = subprocess.check_output(['df', '-h', '/']).decode('utf-8')
        usage_line = df_output.split('\n')[1]
        usage_percent = int(usage_line.split()[4].rstrip('%'))
        
        if usage_percent >= 50:
            print(f"WARNING: Disk usage is at {usage_percent}%, which is above 50%")
            print("Consider cleaning up more space!")
        else:
            print(f"Disk usage is at {usage_percent}%, which is healthy")
            
        print("Aaaaand we're done...")

    except:
        
        print("There was an error processing entry: %s" % (branch,))
        print("Error was: %s" % sys.exc_info()[0])
        raise
    
# Setup the filesystem
if __name__ == "__main__":
    # execute only if run as a script
    args = args = sys.argv[1:]
    launch_setup(args[0])



